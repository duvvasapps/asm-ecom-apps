import React ,{Component}from 'react' ;
import 'react-slideshow-image/dist/styles.css'
import { Slide } from 'react-slideshow-image';
import './header.css';

  class  Product extends Component {

      constructor(props) {
          super(props)
            this.state = {
              products: []
            }
        }
      componentDidMount() {
          fetch('http://localhost:3000/asr-ecom-app/product')
              .then(res => res.json())
              .then((data) => {
                  this.setState({ products: data })
              })
              .catch(console.log)
      }
  render() {
          return (
                <div>
                  <h1 className = "text-center"> Product List</h1>
                  <table className = "table table-striped card card-body">

                    <thead align="center">
                    <div className="slide-container">
        <Slide>
          <div className="each-slide ">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/1.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/2.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
           <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
           <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div> <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
           <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
           <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div> <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
          <div className="each-slide">
            <div style={{'backgroundImage': `url(${window.location.origin + "/img/3.jpg"})`,width:"450px", height:"300px"}}>
              
            </div>
          </div>
        </Slide>
      </div>
                    </thead>
                    <tbody>
                    {this.state.products.map((product1) => (
                      <td key = {product1.id}>
                        <tr > <b> {product1.productType}</b>  -{product1.productName}</tr>   
                                      <tr> <img  style={{display: "block", margin: "0 auto 10px", maxHeight: "200px"}} className="img-fluid"  src={window.location.origin + product1.productImage} alt="product" ></img></tr>   
                                     <tr> RS .{product1.productPrice}/.   {product1.productOffer}</tr>   
                                     

                      </td>
                   
                  ))}
                    </tbody>
                  </table>
                 
                </div>
              )
      }
  }
  

    export default Product