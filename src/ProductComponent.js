import React from 'react';
import ProductService from './ProductService';

class ProductComponent extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            getproducts:[]
        }
    }

    componentDidMount(){
        ProductService.getproducts().then((response) => { console.log(response);
            this.setState({ getproducts: response.data})
        });
    }

    render (){
        return (
            <div>
                <h1 className = "text-center"> Users List</h1>
                <table className = "table table-striped">
                    <thead>
                        <tr>

                            <td> product Id</td>
                            <td> productName </td>
                            <td> productType</td>
                            <td> productOffer</td>
                        </tr>

                    </thead>
                    <tbody>
                        {
                            this.state.getproducts.map(
                                getproducts => 
                                <tr key = {getproducts.id}>
                                     <td> {getproducts.productName}</td>   
                                     <td> {getproducts.productType}</td>   
                                     <td> {getproducts.productPrice}</td>   
                                     <td> {getproducts.productOffer}</td>   
                                </tr>
                            )
                        }

                    </tbody>
                </table>

            </div>

        )
    }
}

export default ProductComponent