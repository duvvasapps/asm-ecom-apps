import axios from 'axios'

const USERS_REST_API_URL = 'http://localhost:6060/asr-ecom-app/getproducts';

class ProductService {

    getproducts(){
        return axios.get(USERS_REST_API_URL);
    }
}

export default new ProductService();