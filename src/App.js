import React, {Component} from 'react';
import './App.css';
import Product from './components/products';
import Header from './components/header';


class  App extends Component {

render() {
        return (
            <div><div><Header /></div>
            <div><Product /></div></div>
        )
    }

}

export default App;
